#!/usr/bin/env python
from __future__ import division


# Main program
def main():
    # Unit selection
    selection = int(raw_input(u'Select your units\n1. kg\n2. lbs\n') or u"0")

    # If metric units are selected:
    if selection == 1:
        # set of things specific for selected unit
        # unit, unit short, ZFW example, OEW, fuel example, one wing max fuel, max TOW
        variables = [u'kilograms', u'kg', 56700, 41413, 6314, 3907.1, 79180]
    # If 3rd world units are selected:
    elif selection == 2:
        variables = [u'pounds', u'lbs', 151800, 91300, 13926, 8613.8, 174200]
    # if you screw up choosing units
    else:
        print u"Try again"
        return

    zfw = float(raw_input(u'Enter ZFW in {} (e.g. {}): '.format(variables[0], variables[2])) or u"0")

    # If ZFW is smaller than aircraft empty weight
    if zfw < variables[4]:
        print u"ZFW too small! ({} {} is smaller than {} {})".format(zfw, variables[1], variables[3], variables[1])
        return

    fuel = float(raw_input(u'Enter fuel weight in {} (e.g. {}): '.format(variables[0], variables[4])) or u"0")

    # Is the plane too heavy for takeoff?
    if fuel + zfw > variables[6]:
        print u'---------------------\nDO NOT TAKEOFF.\nYour aircraft is too heavy for takeoff!\nMAX WEIGHT {} {}, YOUR WEIGHT {} {}\n---------------------\nLoad values are hidden for flight safety.'.format(variables[6], variables[1], fuel + zfw, variables[1])
        return

    # Payload calculation by subtracting aircraft weight
    payload = zfw - variables[3]
    print u"-----\nYour payload weight is", payload, variables[1]

    # Are wing tanks full?
    if fuel > variables[5] * 2:
        # "Function" to calc fuel in the center tank when wing tanks are full
        center = fuel - variables[5] * 2
        print u'Wing tanks - full ({} {} in each), Center tank - {} {}'.format(variables[5], variables[1], center, variables[1])
    # If fuel exactly fits into wing tanks
    elif fuel == variables[5] * 2:
        print u"Wing tanks - full ({} {} in each), Center tank - empty.".format(variables[5], variables[1])
    # If you screw up
    elif fuel == 0:
        print u"Fuel can't be 0"
        return
    else:
        # "Function" to calc fuel in the wing tanks when center tanks is empty
        wings = fuel / 2
        print u"Wing tanks -", wings, variables[1], u"in each, center tank - empty."

    # Output for FUEL AND WEIGHT page in ZIBO B738
    print u"-----\nValues for ZIBO B738 load menu:\nPAYLOAD {:0.2f}\nFUEL    {:0.2f}".format(payload / 1000, fuel / 1000)

if __name__ == u'__main__':
    print u"Weights calculator for X11 B738 v1.4 PYTHON v2 version\nIf something is not working, try regular PYTHON v3 version instead."
    main()
    while True:
        again = raw_input(u"\nProgram finished. Restart (y/n)? ")
        if again == u"y":
            main()
        else:
            quit()
