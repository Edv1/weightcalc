#!/usr/bin/env python3


# Main program
def main():
    # Unit selection
    selection = int(input('Select your units\n1. kg\n2. lbs\n') or "0")

    # If metric units are selected:
    if selection == 1:
        # set of things specific for selected unit
        # unit, unit short, ZFW example, OEW, fuel example, one wing max fuel, max TOW
        variables = ['kilograms', 'kg', 56700, 41413, 6314, 3907.1, 79180]
    # If 3rd world units are selected:
    elif selection == 2:
        variables = ['pounds', 'lbs', 151800, 91300, 13926, 8613.8, 174200]
    # if you screw up choosing units
    else:
        print("Try again")
        return

    zfw = float(input('Enter ZFW in {} (e.g. {}): '.format(variables[0], variables[2])) or "0")

    # If ZFW is smaller than aircraft empty weight
    if zfw < variables[4]:
        print("ZFW too small! ({} {} is smaller than {} {})".format(zfw, variables[1], variables[3], variables[1]))
        return

    fuel = float(input('Enter fuel weight in {} (e.g. {}): '.format(variables[0], variables[4])) or "0")

    # Is the plane too heavy for takeoff?
    if fuel + zfw > variables[6]:
        print('---------------------\nDO NOT TAKEOFF.\nYour aircraft is too heavy for takeoff!\
             \nMAX WEIGHT {} {}, YOUR WEIGHT {} {}\
             \n---------------------\nLoad values are hidden for flight safety.'
             .format(variables[6], variables[1], fuel + zfw, variables[1]))
        return

    # Payload calculation by subtracting aircraft weight
    payload = zfw - variables[3]
    print("-----\nYour payload weight is", payload, variables[1])

    # Are wing tanks full?
    if fuel > variables[5] * 2:
        # "Function" to calc fuel in the center tank when wing tanks are full
        center = fuel - variables[5] * 2
        print('Wing tanks - full ({} {} in each), Center tank - {} {}'.format(variables[5], variables[1], center, variables[1]))
    # If fuel exactly fits into wing tanks
    elif fuel == variables[5] * 2:
        print("Wing tanks - full ({} {} in each), Center tank - empty.".format(variables[5], variables[1]))
    # If you screw up
    elif fuel == 0:
        print("Fuel can't be 0")
        return
    else:
        # "Function" to calc fuel in the wing tanks when center tanks is empty
        wings = fuel / 2
        print("Wing tanks -", wings, variables[1], "in each, center tank - empty.")

    # Output for FUEL AND WEIGHT page in ZIBO B738
    print("-----\nValues for ZIBO B738 load menu:\nPAYLOAD {:0.2f}\nFUEL    {:0.2f}".format(payload / 1000, fuel / 1000))

if __name__ == '__main__':
    print("Weights calculator for X11 B738 v1.4")
    main()
    while True:
        again = input("\nProgram finished. Restart (y/n)? ")
        if again == "y":
            main()
        else:
            quit()
