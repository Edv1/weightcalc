# Weightcalc.py

***weightcalc.py*** - standalone weight and fuel settings calculator for X-Plane 11 Zibo B737-800 and default B738.

Have you ever been annoyed that you have to calculate aircraft payload weight by subtracting empty weight from ZFW? Have you ever been annoyed that you have to calculate how much fuel goes into which tank, since total fuel weight slider puts fuel in all three tanks at once? weightcalc.py solves both of these problems!

## Features:
* Can calculate using both kilograms and pounds;
* Checks if plane is overweight for takeoff.

### For zibo mod:
* Tells your PAYLOAD and FUEL values for the load menu.

### For default B378:
* Can tell you your payload weight based on zero fuel weight entered into weightcalc;
* Can tell you how much fuel you have to put in your center tank when wing tanks are full;
* Can tell you exactly how much fuel do you have to put in each wing tank when they are not full.

## How-to:
1. Extract weightcalc from the archive somewhere;
2. Launch it:
    * ***Windows:*** Double-click weightcalc.py (or weightcalc-python2.py if using python v2);
    * ***Linux-based OS:*** Try double-clicking it first. If that fails, open terminal in the same folder as weightcalc.py, then type ```./weightcalc.py```. If doesn't work, type ```chmod 755 weightcalc.py``` in the terminal, then try again. If still doesn't work, try the same with weightcalc-python2.py instead *(though you really should know how to launch a python program if you are using Linux-based OS)*;
3. Choose your units;
4. Enter ZFW and fuel;
5. Enter results into X-Plane or Zibo load menu.

## Example:
You generate your flight plan via SimBrief or other similar tool. You take ZFW and block fuel from the flight plan and enter it into weightcalc. Weightcalc checks if your plane isn't overweight for takeoff, and outputs your payload, fuel locations (how much fuel goes to which tank), and convenient values for ZIBO load menu (e.g. in tonnes instead of kilograms if metric units are selected).

## Requirements:
* Python v3 installed (Python v2 version is included as well)
